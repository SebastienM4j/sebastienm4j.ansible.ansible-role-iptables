Ansible Role Iptables
=====================

Installs an iptables-based firewall for Linux.

Supports only IPv4 (iptables) for the moment.


Usage
-----

### Role Variables

Available variables are listed below, along with default values (see [`defaults/main.yml`](defaults/main.yml)).

#### Incoming rules

##### `firewall_in_ping: true`

Add incoming rule to allow ping :

```shell
# ICMP (ping)
iptables -A INPUT -p icmp --icmp-type echo-request -j ACCEPT
iptables -A OUTPUT -p icmp --icmp-type echo-reply -j ACCEPT
```

##### `firewall_in_ssh: true`
##### `firewall_in_ssh_port: 22`

Add incoming rule to allow ssh with default port `22` :

```shell
# SSH
iptables -A INPUT -p tcp --dport {{ firewall_in_ssh_port }} -m state --state NEW,ESTABLISHED -j ACCEPT
# FIXME problème de démarrage du service via Ansible avec cette règle (pas de retour suite à la commande de démarrage)
# iptables -A OUTPUT -p tcp --sport {{ firewall_in_ssh_port }} -m state --state ESTABLISHED -j ACCEPT
iptables -A OUTPUT -p tcp --sport {{ firewall_in_ssh_port }} -j ACCEPT
```

##### `firewall_in_http: false`

Add incoming rule to allow HTTP/HTTPS :

```shell
# HTTP/HTTPS
iptables -A INPUT -p tcp -m multiport --dports 80,443 -m state --state NEW,ESTABLISHED -j ACCEPT
iptables -A OUTPUT -p tcp -m multiport --sports 80,443 -m state --state ESTABLISHED -j ACCEPT
```

##### `firewall_in_tcp_ports: []`

List of TCP ports allowed for input trafic. The list is grouped and the group generate a comment line in firewall script.

```yaml
firewall_in_tcp_ports:
  HTTP Tomcat:
    - 8080
```

will generate this :

```shell
# HTTP Tomcat
iptables -A INPUT -p tcp --dport 8080 -m state --state NEW,ESTABLISHED -j ACCEPT
iptables -A OUTPUT -p tcp --sport 8080 -m state --state ESTABLISHED -j ACCEPT
```

##### `firewall_in_rules: []`

List of other incoming rules. The list is grouped and the group generate a comment line in firewall script.

```yaml
firewall_in_rules:
  Other incoming rules:
    - "-A INPUT -p tcp --dport 2222 -m state --state NEW,ESTABLISHED -j ACCEPT"
    - "-A OUTPUT -p tcp --sport 2222 -m state --state ESTABLISHED -j ACCEPT"
```

will generate this :

```shell
# Other incoming rules
iptables -A INPUT -p tcp --dport 2222 -m state --state NEW,ESTABLISHED -j ACCEPT
iptables -A OUTPUT -p tcp --sport 2222 -m state --state ESTABLISHED -j ACCEPT
```

#### Outgoing rules

##### `firewall_out_ntp: true`

Add outgoing rule for NTP (time synchronization) :

```shell
# NTP (server time) 
iptables -A OUTPUT -p udp --dport 123 -j ACCEPT
```

##### `firewall_out_dns: true`

Add outgoing rule for DNS :

```shell
# DNS
iptables -A OUTPUT -p udp --dport 53 -j ACCEPT
iptables -A INPUT -p udp --sport 53 -j ACCEPT
```

##### `firewall_out_http: true`

Add outgoing rule for HTTP/HTTPS :

```shell
# HTTP/HTTPS
iptables -A OUTPUT -p tcp -m multiport --dports 80,443 -m state --state NEW,ESTABLISHED -j ACCEPT
iptables -A INPUT -p tcp -m multiport --sports 80,443 -m state --state ESTABLISHED -j ACCEPT
```

##### `firewall_out_http_squid: false`

Add outgoing rule for HTTP/HTTPS only through squid proxy :

```shell
# HTTP/HTTPS (through squid proxy)
iptables -A OUTPUT -p tcp -m multiport --dports 80,443 -m state --state NEW,ESTABLISHED -m owner --uid-owner proxy -j ACCEPT
iptables -A INPUT -p tcp -m multiport --sports 80,443 -m state --state ESTABLISHED -j ACCEPT
```

##### `firewall_out_smtp: false`

Add outgoing rule for SMTP :

```shell
# SMTP
iptables -A OUTPUT -p tcp --dport 25 -j ACCEPT
```

##### `firewall_out_tcp_ports: []`

List of TCP ports allowed for output trafic. The list is grouped and the group generate a comment line in firewall script.

```yaml
firewall_out_tcp_ports:
  HTTP extra ports:
    - 8080
```

will generate this :

```shell
# HTTP extra ports
iptables -A OUTPUT -p tcp --dport 8080 -m state --state NEW,ESTABLISHED -j ACCEPT
iptables -A INPUT -p tcp --sport 8080 -m state --state ESTABLISHED -j ACCEPT
```

##### `firewall_out_rules: []`

List of other outgoing rules. The list is grouped and the group generate a comment line in firewall script.

```yaml
firewall_out_rules:
  Other incoming rules:
    - "-A OUTPUT -p tcp -m multiport --dports 89,449 -m state --state NEW,ESTABLISHED -j ACCEPT"
    - "-A INPUT -p tcp -m multiport --sports 89,449 -m state --state ESTABLISHED -j ACCEPT"
```

will generate this :

```shell
# Other incoming rules
iptables -A OUTPUT -p tcp -m multiport --dports 89,449 -m state --state NEW,ESTABLISHED -j ACCEPT
iptables -A INPUT -p tcp -m multiport --sports 89,449 -m state --state ESTABLISHED -j ACCEPT
```

##### `firewall_service_state: started`

State of the firewall service.

##### `firewall_service_enabled_at_boot: true`

Enable the firewall service at boot.

#### `firewall_service_notify_restart: false`

Force the restart of firewall service after all tasks are executed.


### Example Playbook

```yaml
- hosts: server
  roles:
    
    - role: iptables
      firewall_in_tcp_ports:
        HTTP Tomcat:
          - 8080
        HTTP Spring Boot Apps:
          - 8081
          - 8082
      firewall_in_rules:
        Other incoming rules:
          - "-A INPUT -p tcp --dport 2222 -m state --state NEW,ESTABLISHED -j ACCEPT"
          - "-A OUTPUT -p tcp --sport 2222 -m state --state ESTABLISHED -j ACCEPT"
      firewall_out_tcp_ports:
        HTTP Without RP:
          - 8080
          - 8088
      firewall_out_rules:
        Other outgoing rules:
          - "-A OUTPUT -p tcp -m multiport --dports 89,449 -m state --state NEW,ESTABLISHED -j ACCEPT"
          - "-A INPUT -p tcp -m multiport --sports 89,449 -m state --state ESTABLISHED -j ACCEPT"
      firewall_service_state: stopped
      firewall_service_enabled_at_boot: false
```


Credits
-------

Inspired by https://github.com/geerlingguy/ansible-role-firewall.


License
-------

Ansible Role Iptables is release under [MIT licence](LICENSE).
